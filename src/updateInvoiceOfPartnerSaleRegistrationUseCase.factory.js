(function () {
    angular
        .module("partnerSaleRegistrationServiceSdk.module")
        .factory(
        "partnerSaleRegistrationServiceSdk.updateInvoiceOfPartnerSaleRegistrationUseCase",
        [
            "partnerSaleRegistrationServiceSdk.config",
            "$http",
            "$q",
            updateInvoiceOfPartnerSaleRegistrationUseCase
        ]);

    function updateInvoiceOfPartnerSaleRegistrationUseCase(config,
                                                           $http,
                                                           $q) {

        return {
            execute: execute
        };

        /**
         * Updates the invoice of a partner sale registration
         * @param {string} partnerSaleRegistrationId
         * @param {string} invoiceId
         */
        function execute(partnerSaleRegistrationId,
                         invoiceId) {
            // use dummy data for now
            var deferred = $q.defer();
            deferred.resolve();
            return deferred.promise;
        }
    }
})
();
