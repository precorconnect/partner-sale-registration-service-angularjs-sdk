(function () {
    angular
        .module("partnerSaleRegistrationServiceSdk.module")
        .factory(
        "partnerSaleRegistrationServiceSdk.addPartnerConsumerSaleRegistrationUseCase",
        [
            "partnerSaleRegistrationServiceSdk.config",
            "$http",
            "$q",
            addPartnerConsumerSaleRegistrationUseCase
        ]);

    function addPartnerConsumerSaleRegistrationUseCase(config,
                                                       $http,
                                                       $q) {

        return {
            execute: execute
        };

        /**
         * Adds a new partner consumer sale registration
         * @param {PartnerConsumerSale} partnerConsumerSale
         * @returns {number} - a  registration id
         */
        function execute(partnerConsumerSale) {
            // use dummy data for now
            var deferred = $q.defer();
            partnerConsumerSale.id = new Date().getTime();
            partnerConsumerSale.createdTimestamp = Math.floor(new Date().getTime() / 1000);
            deferred.resolve((new Date().getTime()));
            return deferred.promise;
        }
    }
})
();
