(function () {
    angular
        .module("partnerSaleRegistrationServiceSdk.module")
        .factory(
        "partnerSaleRegistrationServiceSdk.listPartnerCommercialSaleRegistrationsUseCase",
        [
            "partnerSaleRegistrationServiceSdk.config",
            "$http",
            "$q",
            listPartnerCommercialSaleRegistrationsUseCase
        ]);

    function listPartnerCommercialSaleRegistrationsUseCase(config,
                                                           $http,
                                                           $q) {

        return {
            execute: execute
        };

        /**
         * Lists partner commercial sale registrations meeting the provided filter criteria which may include:
         * -  occurred since timestamp
         * -  partner sap account number
         * @param {number} timestamp
         * @param {string} partnerSapAccountNumber
         * @returns {promise} of {PartnerCommercialSaleRegistrationSynopsisView[]}
         */
        function execute(timestamp,
                         partnerSapAccountNumber) {

            // use dummy data for now
            var deferred = $q.defer();
            deferred.resolve(
                [
                    {
                        sale: {
                            facilityId: "001K000001A3qBy",
                            invoiceId: "3"
                        },
                        createdTimestamp: 1432701017,
                        id: "234213423"
                    },
                    {
                        sale: {
                            facilityId: "001A000000L0uKt",
                            invoiceId: "2"
                        },
                        createdTimestamp: 1426706017,
                        id: "224234423"
                    },
                    {
                        sale: {
                            facilityId: "001A000000L0tbC",
                            invoiceId: "1"
                        },
                        createdTimestamp: 1438706017,
                        id: "224234423"
                    }
                ]);
            return deferred.promise;
        }
    }
})
();
