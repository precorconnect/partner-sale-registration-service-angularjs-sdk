(function () {
    angular
        .module("partnerSaleRegistrationServiceSdk.module")
        .factory(
        "partnerSaleRegistrationServiceSdk.constructPartnerSaleCompositeLineItemUseCase",
        [
            "partnerSaleRegistrationServiceSdk.config",
            "$http",
            "$q",
            constructPartnerSaleCompositeLineItemUseCase
        ]);

    function constructPartnerSaleCompositeLineItemUseCase(config,
                                                   $http,
                                                   $q) {

        return {
            execute: execute
        };

        /**
         * Constructs a PartnerSaleCompositeLineItem
         * @returns {PartnerSaleCompositeLineItem}
         */
        function execute() {
            return {
                price: null,
                components: []
            };
        }
    }
})
();
