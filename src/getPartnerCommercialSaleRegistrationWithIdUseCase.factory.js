(function () {
    angular
        .module("partnerSaleRegistrationServiceSdk.module")
        .factory(
        "partnerSaleRegistrationServiceSdk.getPartnerCommercialSaleRegistrationWithIdUseCase",
        [
            "partnerSaleRegistrationServiceSdk.config",
            "$http",
            "$q",
            getPartnerCommercialSaleRegistrationWithIdUseCase
        ]);

    function getPartnerCommercialSaleRegistrationWithIdUseCase(config,
                                                               $http,
                                                               $q) {

        return {
            execute: execute
        };

        /**
         * Gets the partner commercial sale registration with the provided id
         * @param id
         * @returns {promise} of {PartnerCommercialSaleRegistrationView}
         */
        function execute(id) {
            // use dummy data for now
            var deferred = $q.defer();
            deferred.resolve(
                {
                    sale: {
                        createdTimestamp: 234234234,
                        invoiceId: "1",
                        customerSourceId: "Web",
                        managementCompanyId: "Xyz Management",
                        buyingGroupId: "ABC Buying Group",
                        facilityId: "001K000001A3qBy",
                        facilityContactId: "003A000000Mb5bo",
                        repId: "1",
                        lineItems: [
                            {
                                serialNumber: "AD23423333",
                                productGroup: "Strength - Commercial",
                                productLine: "Strength Plate Loaded Icarian",
                                price: 200.00
                            },
                            {
                                components: [
                                    {
                                        serialNumber: "A925D17130D01",
                                        productGroup: "Treadmill - Commercial",
                                        productLine: "Treadmill 921",
                                        price: 700.00
                                    },
                                    {
                                        serialNumber: "BD633A00F0DD",
                                        productGroup: "Treadmill Display - Commercial",
                                        productLine: "Treadmill P30 Display - Commercial",
                                        price: 500.00
                                    }
                                ]
                            }
                        ]
                    },
                    id: id,
                    createdTimestamp: 234234234
                });
            return deferred.promise;
        }
    }
})
();
