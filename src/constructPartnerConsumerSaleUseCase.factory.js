(function () {
    angular
        .module("partnerSaleRegistrationServiceSdk.module")
        .factory(
        "partnerSaleRegistrationServiceSdk.constructPartnerConsumerSaleUseCase",
        [
            "partnerSaleRegistrationServiceSdk.config",
            "$http",
            "$q",
            constructPartnerConsumerSaleUseCase
        ]);

    function constructPartnerConsumerSaleUseCase(config,
                                                 $http,
                                                 $q) {

        return {
            execute: execute
        };

        /**
         * Constructs a PartnerConsumerSale
         * @returns {PartnerConsumerSale}
         */
        function execute() {
            return {
                createdTimestamp: null,
                invoiceId: null,
                consumerId: null,
                repId: null,
                lineItems: []
            };
        }
    }
})
();
