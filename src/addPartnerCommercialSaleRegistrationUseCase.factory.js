(function () {
    angular
        .module("partnerSaleRegistrationServiceSdk.module")
        .factory(
        "partnerSaleRegistrationServiceSdk.addPartnerCommercialSaleRegistrationUseCase",
        [
            "partnerSaleRegistrationServiceSdk.config",
            "$http",
            "$q",
            addPartnerCommercialSaleRegistrationUseCase
        ]);

    function addPartnerCommercialSaleRegistrationUseCase(config,
                                                         $http,
                                                         $q) {

        return {execute: execute};

        /**
         * Adds a new partner commercial sale registration
         * @param {PartnerCommercialSale} partnerCommercialSale
         * @returns {number} - a registration id
         */
        function execute(partnerCommercialSale) {
            // use dummy data for now
            var deferred = $q.defer();
            partnerCommercialSale.id = new Date().getTime();
            partnerCommercialSale.createdTimestamp = Math.floor(new Date().getTime() / 1000);
            deferred.resolve((new Date().getTime()));
            return deferred.promise;
        }
    }
})
();
