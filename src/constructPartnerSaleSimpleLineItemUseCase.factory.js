(function () {
    angular
        .module("partnerSaleRegistrationServiceSdk.module")
        .factory(
        "partnerSaleRegistrationServiceSdk.constructPartnerSaleSimpleLineItemUseCase",
        [
            "partnerSaleRegistrationServiceSdk.config",
            "$http",
            "$q",
            constructPartnerSaleSimpleLineItemUseCase
        ]);

    function constructPartnerSaleSimpleLineItemUseCase(config,
                                                       $http,
                                                       $q) {

        return {
            execute: execute
        };

        /**
         * Constructs a PartnerSaleSimpleLineItem
         * @returns {PartnerSaleSimpleLineItem}
         */
        function execute() {
            return {
                productGroupId: null,
                productLineId: null,
                serialNumber: null,
                description: null,
                price: null
            };
        }
    }
})
();
