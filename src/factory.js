(function () {
    angular
        .module("partnerSaleRegistrationServiceSdk.module")
        .factory(
        "partnerSaleRegistrationServiceSdk",
        [
            "partnerSaleRegistrationServiceSdk.addPartnerCommercialSaleRegistrationUseCase",
            "partnerSaleRegistrationServiceSdk.addPartnerConsumerSaleRegistrationUseCase",
            "partnerSaleRegistrationServiceSdk.constructPartnerCommercialSaleUseCase",
            "partnerSaleRegistrationServiceSdk.constructPartnerConsumerSaleUseCase",
            "partnerSaleRegistrationServiceSdk.constructPartnerSaleCompositeLineItemUseCase",
            "partnerSaleRegistrationServiceSdk.constructPartnerSaleSimpleLineItemUseCase",
            "partnerSaleRegistrationServiceSdk.getPartnerCommercialSaleRegistrationWithIdUseCase",
            "partnerSaleRegistrationServiceSdk.getPartnerConsumerSaleRegistrationWithIdUseCase",
            "partnerSaleRegistrationServiceSdk.listPartnerCommercialSaleRegistrationsUseCase",
            "partnerSaleRegistrationServiceSdk.updateInvoiceOfPartnerSaleRegistrationUseCase",
            partnerSaleRegistrationServiceSdk
        ]);

    function partnerSaleRegistrationServiceSdk(addPartnerCommercialSaleRegistrationUseCase,
                                               addPartnerConsumerSaleRegistrationUseCase,
                                               constructPartnerCommercialSaleUseCase,
                                               constructPartnerConsumerSaleUseCase,
                                               constructPartnerSaleCompositeLineItemUseCase,
                                               constructPartnerSaleSimpleLineItemUseCase,
                                               getPartnerCommercialSaleRegistrationWithIdUseCase,
                                               getPartnerConsumerSaleRegistrationWithIdUseCase,
                                               listPartnerCommercialSaleRegistrationsUseCase,
                                               updateInvoiceOfPartnerSaleRegistrationUseCase) {

        return {
            addPartnerCommercialSaleRegistration: addPartnerCommercialSaleRegistrationUseCase.execute,
            addPartnerConsumerSaleRegistration: addPartnerConsumerSaleRegistrationUseCase.execute,
            constructPartnerCommercialSale: constructPartnerCommercialSaleUseCase.execute,
            constructPartnerConsumerSale: constructPartnerConsumerSaleUseCase.execute,
            constructPartnerSaleCompositeLineItem: constructPartnerSaleCompositeLineItemUseCase.execute,
            constructPartnerSaleSimpleLineItem: constructPartnerSaleSimpleLineItemUseCase.execute,
            getPartnerCommercialSaleRegistrationWithId: getPartnerCommercialSaleRegistrationWithIdUseCase.execute,
            getPartnerConsumerSaleRegistrationWithId: getPartnerConsumerSaleRegistrationWithIdUseCase.execute,
            listPartnerCommercialSaleRegistrations: listPartnerCommercialSaleRegistrationsUseCase.execute,
            updateInvoiceOfPartnerSaleRegistration: updateInvoiceOfPartnerSaleRegistrationUseCase.execute
        };
    }
})
();
