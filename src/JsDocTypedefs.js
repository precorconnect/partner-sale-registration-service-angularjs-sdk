/**
 * A partner sale
 *
 * @typedef {Object} PartnerSale
 * @property {number} createdTimestamp - timestamp of the instant the sale was created/processed
 * @property {string} invoiceId
 * @property {string} repId
 * @property {PartnerSaleLineItem[]} lineItems
 */

/**
 * A partner commercial sale
 *
 * @typedef {Object} PartnerCommercialSale
 * @augments PartnerSale
 * @property {string} customerSourceId
 * @property {string} managementCompanyId
 * @property {string} buyingGroupId
 * @property {string} facilityId
 * @property {string} facilityContactId
 */

/**
 * A partner consumer sale
 *
 * @typedef {Object} PartnerConsumerSale
 * @augments PartnerSale
 * @property {number} consumerId
 */

/**
 * The least detailed view of a PartnerSale
 *
 * @typedef {Object} PartnerSaleSynopsisView
 * @property {number} createdTimestamp - timestamp of the instant the registration was created/processed
 * @property {string} invoiceId
 */

/**
 * The least detailed view of a PartnerCommercialSale
 *
 * @typedef {Object} PartnerCommercialSaleSynopsisView
 * @augments PartnerSaleSynopsisView
 * @property {string} facilityId
 */

/**
 * @typedef {Object} PartnerSaleLineItem
 * @property {number} [price]
 */

/**
 * @typedef {Object} PartnerSaleCompositeLineItem
 * @augments PartnerSaleLineItem
 * @property {PartnerSaleSimpleLineItem[]} [components]
 */

/**
 * @typedef {Object} PartnerSaleSimpleLineItem
 * @augments PartnerSaleLineItem
 * @property {string} productGroupId
 * @property {string} productLineId
 * @property {string} serialNumber
 * @property {string} description
 */

/**
 *  The most detailed view of a partner sale registration
 *
 * @typedef {Object} PartnerSaleRegistrationView
 * @property {PartnerSale} sale
 * @property {number} createdTimestamp
 * @property {number} id
 */

/**
 *  The most detailed view of a partner consumer sale registration
 *
 * @typedef {Object} PartnerConsumerSaleRegistrationView
 * @augments {PartnerSaleRegistrationView}
 * @property {PartnerConsumerSale} sale
 */

/**
 * The most detailed view of a partner commercial sale registration
 *
 * @typedef {Object} PartnerCommercialSaleRegistrationView
 * @augments {PartnerSaleRegistrationView}
 * @property {PartnerCommercialSale} sale
 */

/**
 * The least detailed view of a partner commercial sale registration
 * @typedef {Object} PartnerCommercialSaleRegistrationSynopsisView
 * @property {PartnerCommercialSaleSynopsisView} sale
 * @property {number} createdTimestamp
 * @property {number} id
 */