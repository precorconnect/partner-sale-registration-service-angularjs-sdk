(function () {
    angular
        .module("partnerSaleRegistrationServiceSdk.module")
        .factory(
        "partnerSaleRegistrationServiceSdk.constructPartnerCommercialSaleUseCase",
        [
            "partnerSaleRegistrationServiceSdk.config",
            "$http",
            "$q",
            constructPartnerCommercialSaleUseCase
        ]);

    function constructPartnerCommercialSaleUseCase(config,
                                                   $http,
                                                   $q) {

        return {
            execute: execute
        };

        /**
         * Constructs a PartnerCommercialSale
         * @returns {PartnerCommercialSale}
         */
        function execute() {
            return {
                createdTimestamp: null,
                invoiceId: null,
                customerSourceId: null,
                managementCompanyId: null,
                buyingGroupId: null,
                facilityId: null,
                facilityContactId: null,
                repId: null,
                lineItems: []
            };
        }
    }
})
();
