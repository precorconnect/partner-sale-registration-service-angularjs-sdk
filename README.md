## Description
An AngularJS Module implementing common use cases encountered when integrating AngularJS apps
 with the precorconnect partner sale registration service.

## UseCases

#####Construct Partner Commercial Sale Registration
Constructs a PartnerCommercialSale

#####Construct Partner Consumer Sale Registration
Constructs a PartnerConsumerSale

#####Construct Partner Sale Composite Line Item
Constructs a PartnerSaleCompositeLineItem

#####Construct Partner Sale Simple Line Item
Constructs a PartnerSaleSimpleLineItem

#####Get Partner Sale Registration With Id
Gets the partner sale registration with the provided id

#####Get Partner Commercial Sale Registration With Id
Gets the partner commercial sale registration with the provided id

#####Get Partner Consumer Sale Registration With Id
Gets the partner consumer sale registration with the provided id

#####List Partner Commercial Sale Registrations
Lists partner commercial sale registrations meeting the provided filter criteria which may include:
-  occurred since timestamp
-  partner sap account number

#####Add Partner Commercial Sale Registration
Adds a new partner commercial sale registration 

#####Add Partner Consumer Sale Registration
Adds a new partner consumer sale registration

#####Update Invoice Of Partner Sale Registration
Updates the invoice of a partner sale registration

## Installation
**install required dependencies:**  
-  [angular](https://angularjs.org/)  
-  [angularjs-session](https://bitbucket.org/precorconnect/angularjs-session)  

**add bower dependency**  
```shell
bower install https://bitbucket.org/precorconnect/partner-sale-registration-service-angularjs-sdk.git --save
```  

**include in view**  
```html
<script src="bower-components/partner-sale-registration-service-angularjs-sdk/dist/partner-sale-registration-service-angularjs-sdk.js"></script>
```  

**add to angular dependencies**
```js
angular.module(
        "app",
        ["partnerSaleRegistrationServiceSdk.module"]);
```
configure  
see below.

## Configuration
####Properties
| Name (* denotes required) | Description |
|------|-------------|
| baseUrl* | The base url of the partner sale registration service. |

#### Example
```js
angular.module(
        "app",
        ["partnerSaleRegistrationServiceSdk.module"])
        .config(
        [
            "partnerSaleRegistrationServiceSdk.configProvider",
            appConfig
        ]);

    function appConfig(partnerSaleRegistrationServiceSdk_configProvider) {
        partnerSaleRegistrationServiceSdk_configProvider
            .setBaseUrl("@@partnerSaleRegistrationServiceBaseUrl");
    }
```