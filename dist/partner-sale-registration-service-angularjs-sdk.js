/**
 * A partner sale
 *
 * @typedef {Object} PartnerSale
 * @property {number} createdTimestamp - timestamp of the instant the sale was created/processed
 * @property {string} invoiceId
 * @property {string} repId
 * @property {PartnerSaleLineItem[]} lineItems
 */

/**
 * A partner commercial sale
 *
 * @typedef {Object} PartnerCommercialSale
 * @augments PartnerSale
 * @property {string} customerSourceId
 * @property {string} managementCompanyId
 * @property {string} buyingGroupId
 * @property {string} facilityId
 * @property {string} facilityContactId
 */

/**
 * A partner consumer sale
 *
 * @typedef {Object} PartnerConsumerSale
 * @augments PartnerSale
 * @property {number} consumerId
 */

/**
 * The least detailed view of a PartnerSale
 *
 * @typedef {Object} PartnerSaleSynopsisView
 * @property {number} createdTimestamp - timestamp of the instant the registration was created/processed
 * @property {string} invoiceId
 */

/**
 * The least detailed view of a PartnerCommercialSale
 *
 * @typedef {Object} PartnerCommercialSaleSynopsisView
 * @augments PartnerSaleSynopsisView
 * @property {string} facilityId
 */

/**
 * @typedef {Object} PartnerSaleLineItem
 * @property {number} [price]
 */

/**
 * @typedef {Object} PartnerSaleCompositeLineItem
 * @augments PartnerSaleLineItem
 * @property {PartnerSaleSimpleLineItem[]} [components]
 */

/**
 * @typedef {Object} PartnerSaleSimpleLineItem
 * @augments PartnerSaleLineItem
 * @property {string} productGroupId
 * @property {string} productLineId
 * @property {string} serialNumber
 * @property {string} description
 */

/**
 *  The most detailed view of a partner sale registration
 *
 * @typedef {Object} PartnerSaleRegistrationView
 * @property {PartnerSale} sale
 * @property {number} createdTimestamp
 * @property {number} id
 */

/**
 *  The most detailed view of a partner consumer sale registration
 *
 * @typedef {Object} PartnerConsumerSaleRegistrationView
 * @augments {PartnerSaleRegistrationView}
 * @property {PartnerConsumerSale} sale
 */

/**
 * The most detailed view of a partner commercial sale registration
 *
 * @typedef {Object} PartnerCommercialSaleRegistrationView
 * @augments {PartnerSaleRegistrationView}
 * @property {PartnerCommercialSale} sale
 */

/**
 * The least detailed view of a partner commercial sale registration
 * @typedef {Object} PartnerCommercialSaleRegistrationSynopsisView
 * @property {PartnerCommercialSaleSynopsisView} sale
 * @property {number} createdTimestamp
 * @property {number} id
 */
(function () {
    angular
        .module("partnerSaleRegistrationServiceSdk.module")
        .factory(
        "partnerSaleRegistrationServiceSdk.addPartnerCommercialSaleRegistrationUseCase",
        [
            "partnerSaleRegistrationServiceSdk.config",
            "$http",
            "$q",
            addPartnerCommercialSaleRegistrationUseCase
        ]);

    function addPartnerCommercialSaleRegistrationUseCase(config,
                                                         $http,
                                                         $q) {

        return {execute: execute};

        /**
         * Adds a new partner commercial sale registration
         * @param {PartnerCommercialSale} partnerCommercialSale
         * @returns {number} - a registration id
         */
        function execute(partnerCommercialSale) {
            // use dummy data for now
            var deferred = $q.defer();
            partnerCommercialSale.id = new Date().getTime();
            partnerCommercialSale.createdTimestamp = Math.floor(new Date().getTime() / 1000);
            deferred.resolve((new Date().getTime()));
            return deferred.promise;
        }
    }
})
();

(function () {
    angular
        .module("partnerSaleRegistrationServiceSdk.module")
        .factory(
        "partnerSaleRegistrationServiceSdk.addPartnerConsumerSaleRegistrationUseCase",
        [
            "partnerSaleRegistrationServiceSdk.config",
            "$http",
            "$q",
            addPartnerConsumerSaleRegistrationUseCase
        ]);

    function addPartnerConsumerSaleRegistrationUseCase(config,
                                                       $http,
                                                       $q) {

        return {
            execute: execute
        };

        /**
         * Adds a new partner consumer sale registration
         * @param {PartnerConsumerSale} partnerConsumerSale
         * @returns {number} - a  registration id
         */
        function execute(partnerConsumerSale) {
            // use dummy data for now
            var deferred = $q.defer();
            partnerConsumerSale.id = new Date().getTime();
            partnerConsumerSale.createdTimestamp = Math.floor(new Date().getTime() / 1000);
            deferred.resolve((new Date().getTime()));
            return deferred.promise;
        }
    }
})
();

(function () {
    angular
        .module("partnerSaleRegistrationServiceSdk.module")
        .provider(
        'partnerSaleRegistrationServiceSdk.config',
        configProvider
    );

    function configProvider() {

        var objectUnderConstruction = {
            setBaseUrl: setBaseUrl,
            $get: $get
        };

        return objectUnderConstruction;

        function setBaseUrl(baseUrl) {
            objectUnderConstruction.baseUrl = baseUrl;
            return objectUnderConstruction;
        }

        function $get() {
            return {
                baseUrl: objectUnderConstruction.baseUrl
            }
        }
    }
})();
(function () {
    angular
        .module("partnerSaleRegistrationServiceSdk.module")
        .factory(
        "partnerSaleRegistrationServiceSdk.constructPartnerCommercialSaleUseCase",
        [
            "partnerSaleRegistrationServiceSdk.config",
            "$http",
            "$q",
            constructPartnerCommercialSaleUseCase
        ]);

    function constructPartnerCommercialSaleUseCase(config,
                                                   $http,
                                                   $q) {

        return {
            execute: execute
        };

        /**
         * Constructs a PartnerCommercialSale
         * @returns {PartnerCommercialSale}
         */
        function execute() {
            return {
                createdTimestamp: null,
                invoiceId: null,
                customerSourceId: null,
                managementCompanyId: null,
                buyingGroupId: null,
                facilityId: null,
                facilityContactId: null,
                repId: null,
                lineItems: []
            };
        }
    }
})
();

(function () {
    angular
        .module("partnerSaleRegistrationServiceSdk.module")
        .factory(
        "partnerSaleRegistrationServiceSdk.constructPartnerConsumerSaleUseCase",
        [
            "partnerSaleRegistrationServiceSdk.config",
            "$http",
            "$q",
            constructPartnerConsumerSaleUseCase
        ]);

    function constructPartnerConsumerSaleUseCase(config,
                                                 $http,
                                                 $q) {

        return {
            execute: execute
        };

        /**
         * Constructs a PartnerConsumerSale
         * @returns {PartnerConsumerSale}
         */
        function execute() {
            return {
                createdTimestamp: null,
                invoiceId: null,
                consumerId: null,
                repId: null,
                lineItems: []
            };
        }
    }
})
();

(function () {
    angular
        .module("partnerSaleRegistrationServiceSdk.module")
        .factory(
        "partnerSaleRegistrationServiceSdk.constructPartnerSaleCompositeLineItemUseCase",
        [
            "partnerSaleRegistrationServiceSdk.config",
            "$http",
            "$q",
            constructPartnerSaleCompositeLineItemUseCase
        ]);

    function constructPartnerSaleCompositeLineItemUseCase(config,
                                                   $http,
                                                   $q) {

        return {
            execute: execute
        };

        /**
         * Constructs a PartnerSaleCompositeLineItem
         * @returns {PartnerSaleCompositeLineItem}
         */
        function execute() {
            return {
                price: null,
                components: []
            };
        }
    }
})
();

(function () {
    angular
        .module("partnerSaleRegistrationServiceSdk.module")
        .factory(
        "partnerSaleRegistrationServiceSdk.constructPartnerSaleSimpleLineItemUseCase",
        [
            "partnerSaleRegistrationServiceSdk.config",
            "$http",
            "$q",
            constructPartnerSaleSimpleLineItemUseCase
        ]);

    function constructPartnerSaleSimpleLineItemUseCase(config,
                                                       $http,
                                                       $q) {

        return {
            execute: execute
        };

        /**
         * Constructs a PartnerSaleSimpleLineItem
         * @returns {PartnerSaleSimpleLineItem}
         */
        function execute() {
            return {
                productGroupId: null,
                productLineId: null,
                serialNumber: null,
                description: null,
                price: null
            };
        }
    }
})
();

(function () {
    angular
        .module("partnerSaleRegistrationServiceSdk.module")
        .factory(
        "partnerSaleRegistrationServiceSdk",
        [
            "partnerSaleRegistrationServiceSdk.addPartnerCommercialSaleRegistrationUseCase",
            "partnerSaleRegistrationServiceSdk.addPartnerConsumerSaleRegistrationUseCase",
            "partnerSaleRegistrationServiceSdk.constructPartnerCommercialSaleUseCase",
            "partnerSaleRegistrationServiceSdk.constructPartnerConsumerSaleUseCase",
            "partnerSaleRegistrationServiceSdk.constructPartnerSaleCompositeLineItemUseCase",
            "partnerSaleRegistrationServiceSdk.constructPartnerSaleSimpleLineItemUseCase",
            "partnerSaleRegistrationServiceSdk.getPartnerCommercialSaleRegistrationWithIdUseCase",
            "partnerSaleRegistrationServiceSdk.getPartnerConsumerSaleRegistrationWithIdUseCase",
            "partnerSaleRegistrationServiceSdk.listPartnerCommercialSaleRegistrationsUseCase",
            "partnerSaleRegistrationServiceSdk.updateInvoiceOfPartnerSaleRegistrationUseCase",
            partnerSaleRegistrationServiceSdk
        ]);

    function partnerSaleRegistrationServiceSdk(addPartnerCommercialSaleRegistrationUseCase,
                                               addPartnerConsumerSaleRegistrationUseCase,
                                               constructPartnerCommercialSaleUseCase,
                                               constructPartnerConsumerSaleUseCase,
                                               constructPartnerSaleCompositeLineItemUseCase,
                                               constructPartnerSaleSimpleLineItemUseCase,
                                               getPartnerCommercialSaleRegistrationWithIdUseCase,
                                               getPartnerConsumerSaleRegistrationWithIdUseCase,
                                               listPartnerCommercialSaleRegistrationsUseCase,
                                               updateInvoiceOfPartnerSaleRegistrationUseCase) {

        return {
            addPartnerCommercialSaleRegistration: addPartnerCommercialSaleRegistrationUseCase.execute,
            addPartnerConsumerSaleRegistration: addPartnerConsumerSaleRegistrationUseCase.execute,
            constructPartnerCommercialSale: constructPartnerCommercialSaleUseCase.execute,
            constructPartnerConsumerSale: constructPartnerConsumerSaleUseCase.execute,
            constructPartnerSaleCompositeLineItem: constructPartnerSaleCompositeLineItemUseCase.execute,
            constructPartnerSaleSimpleLineItem: constructPartnerSaleSimpleLineItemUseCase.execute,
            getPartnerCommercialSaleRegistrationWithId: getPartnerCommercialSaleRegistrationWithIdUseCase.execute,
            getPartnerConsumerSaleRegistrationWithId: getPartnerConsumerSaleRegistrationWithIdUseCase.execute,
            listPartnerCommercialSaleRegistrations: listPartnerCommercialSaleRegistrationsUseCase.execute,
            updateInvoiceOfPartnerSaleRegistration: updateInvoiceOfPartnerSaleRegistrationUseCase.execute
        };
    }
})
();

(function () {
    angular
        .module("partnerSaleRegistrationServiceSdk.module")
        .factory(
        "partnerSaleRegistrationServiceSdk.getPartnerCommercialSaleRegistrationWithIdUseCase",
        [
            "partnerSaleRegistrationServiceSdk.config",
            "$http",
            "$q",
            getPartnerCommercialSaleRegistrationWithIdUseCase
        ]);

    function getPartnerCommercialSaleRegistrationWithIdUseCase(config,
                                                               $http,
                                                               $q) {

        return {
            execute: execute
        };

        /**
         * Gets the partner commercial sale registration with the provided id
         * @param id
         * @returns {promise} of {PartnerCommercialSaleRegistrationView}
         */
        function execute(id) {
            // use dummy data for now
            var deferred = $q.defer();
            deferred.resolve(
                {
                    sale: {
                        createdTimestamp: 234234234,
                        invoiceId: "1",
                        customerSourceId: "Web",
                        managementCompanyId: "Xyz Management",
                        buyingGroupId: "ABC Buying Group",
                        facilityId: "001K000001A3qBy",
                        facilityContactId: "003A000000Mb5bo",
                        repId: "1",
                        lineItems: [
                            {
                                serialNumber: "AD23423333",
                                productGroup: "Strength - Commercial",
                                productLine: "Strength Plate Loaded Icarian",
                                price: 200.00
                            },
                            {
                                components: [
                                    {
                                        serialNumber: "A925D17130D01",
                                        productGroup: "Treadmill - Commercial",
                                        productLine: "Treadmill 921",
                                        price: 700.00
                                    },
                                    {
                                        serialNumber: "BD633A00F0DD",
                                        productGroup: "Treadmill Display - Commercial",
                                        productLine: "Treadmill P30 Display - Commercial",
                                        price: 500.00
                                    }
                                ]
                            }
                        ]
                    },
                    id: id,
                    createdTimestamp: 234234234
                });
            return deferred.promise;
        }
    }
})
();

(function () {
    angular
        .module("partnerSaleRegistrationServiceSdk.module")
        .factory(
        "partnerSaleRegistrationServiceSdk.getPartnerConsumerSaleRegistrationWithIdUseCase",
        [
            "partnerSaleRegistrationServiceSdk.config",
            "$http",
            "$q",
            getPartnerConsumerSaleRegistrationWithIdUseCase
        ]);

    function getPartnerConsumerSaleRegistrationWithIdUseCase(config,
                                                             $http,
                                                             $q) {

        return {execute: execute};

        /**
         * Gets the partner consumer sale registration with the provided id
         * @param id
         * @returns {promise} of {PartnerConsumerSaleRegistrationView}
         */
        function execute(id) {
            // use dummy data for now
            var deferred = $q.defer();
            deferred.resolve({
                sale: {
                    createdTimestamp: 234234234,
                    invoiceId: "1",
                    consumerId: "1",
                    repId: "rep@vendor.com",
                    lineItems: [
                        {
                            serialNumber: "AD23423333",
                            productGroup: "Strength - Commercial",
                            productLine: "Strength Plate Loaded Icarian",
                            price: 200.00
                        },
                        {
                            components: [
                                {
                                    serialNumber: "A925D17130D01",
                                    productGroup: "Treadmill - Commercial",
                                    productLine: "Treadmill 921",
                                    price: 700.00
                                },
                                {
                                    serialNumber: "BD633A00F0DD",
                                    productGroup: "Treadmill Display - Commercial",
                                    productLine: "Treadmill P30 Display - Commercial",
                                    price: 500.00
                                }
                            ]
                        }
                    ]
                },
                id: id,
                createdTimestamp: 234234234
            });
            return deferred.promise;
        }
    }
})
();

(function () {
    angular
        .module("partnerSaleRegistrationServiceSdk.module")
        .factory(
        "partnerSaleRegistrationServiceSdk.listPartnerCommercialSaleRegistrationsUseCase",
        [
            "partnerSaleRegistrationServiceSdk.config",
            "$http",
            "$q",
            listPartnerCommercialSaleRegistrationsUseCase
        ]);

    function listPartnerCommercialSaleRegistrationsUseCase(config,
                                                           $http,
                                                           $q) {

        return {
            execute: execute
        };

        /**
         * Lists partner commercial sale registrations meeting the provided filter criteria which may include:
         * -  occurred since timestamp
         * -  partner sap account number
         * @param {number} timestamp
         * @param {string} partnerSapAccountNumber
         * @returns {promise} of {PartnerCommercialSaleRegistrationSynopsisView[]}
         */
        function execute(timestamp,
                         partnerSapAccountNumber) {

            // use dummy data for now
            var deferred = $q.defer();
            deferred.resolve(
                [
                    {
                        sale: {
                            facilityId: "001K000001A3qBy",
                            invoiceId: "3"
                        },
                        createdTimestamp: 1432701017,
                        id: "234213423"
                    },
                    {
                        sale: {
                            facilityId: "001A000000L0uKt",
                            invoiceId: "2"
                        },
                        createdTimestamp: 1426706017,
                        id: "224234423"
                    },
                    {
                        sale: {
                            facilityId: "001A000000L0tbC",
                            invoiceId: "1"
                        },
                        createdTimestamp: 1438706017,
                        id: "224234423"
                    }
                ]);
            return deferred.promise;
        }
    }
})
();

(function () {
    angular.module(
        "partnerSaleRegistrationServiceSdk.module",
        [
            "session.module"
        ]
    );
})();
(function () {
    angular
        .module("partnerSaleRegistrationServiceSdk.module")
        .factory(
        "partnerSaleRegistrationServiceSdk.updateInvoiceOfPartnerSaleRegistrationUseCase",
        [
            "partnerSaleRegistrationServiceSdk.config",
            "$http",
            "$q",
            updateInvoiceOfPartnerSaleRegistrationUseCase
        ]);

    function updateInvoiceOfPartnerSaleRegistrationUseCase(config,
                                                           $http,
                                                           $q) {

        return {
            execute: execute
        };

        /**
         * Updates the invoice of a partner sale registration
         * @param {string} partnerSaleRegistrationId
         * @param {string} invoiceId
         */
        function execute(partnerSaleRegistrationId,
                         invoiceId) {
            // use dummy data for now
            var deferred = $q.defer();
            deferred.resolve();
            return deferred.promise;
        }
    }
})
();
