module.exports = function (grunt) {
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.initConfig({
        concat:{
            dist: {
                src: ['src/partner-sale-registration-service.module.js','src/*.js'],
                dest: 'dist/partner-sale-registration-service-angularjs-sdk.js'
            }
        }
    });
    grunt.registerTask('default', ['concat']);
};